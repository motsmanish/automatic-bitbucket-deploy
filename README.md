
# Automatic deployment for bitbucket.org web-based projects

Fork of https://bitbucket.org/lilliputten/automatic-bitbucket-deploy/

## Features

- Uses Bitbucket.org webhooks (stream instead of POST)
- Support for multiple projects. See array `$PROJECTS` in **config.sample.php**.
- Project and repository folders are created automaticaly
- Post hook command execution.

## Requirements

- PHP 5.3+;
- Git installed;
- Shell access;
- PHP exec function;
- SSH key pair for bitbucket created with **empty** passphrase;

## Installation

For your server to connect securely to Bitbucket without a password prompt, it needs to use an SSH key.

On your server navigate to the **~/.ssh** directory of the user that PHP runs under. You will need to create the user's .ssh directory if it doesn't exist. At a shell prompt type:

```
cd ~/.ssh
ssh-keygen -t rsa
```

When prompted either accept the default key name (**id_rsa**) or give your key a unique name. Press enter when asked for a passphrase, which will generate a passwordless key. Usually this isn't recommended, but we need our script to be able to connect to Bitbucket without a passphrase.

A public and private key pair will be generated. Copy your public key � the one with a _.pub_ extension � to the clipboard. On the Bitbucket website navigate to _Account > SSH Keys_, and choose to add a new key. Paste in your public key and save it.

Back on your server, edit your **~/.ssh/config** file to add _bitbucket.org_ as a host. This ensures that the correct key is used when connecting by SSH to _bitbucket.org_. You'll need to create the config file if it doesn't exist:

```
Host bitbucket.org
    IdentityFile ~/.ssh/bitbucket_rsa
```

Whenever you do a git fetch Bitbucket will verify your identity automatically, without prompting you for a password.

On the Bitbucket website navigate to your repository's _Administration > Webhooks_ screen and add a new webhook, pointed to `http://<domain>/<path>/bitbucket-hook.php`.

For more detailed information see the [original Jonathan's page](http://jonathannicol.com/blog/2013/11/19/automated-git-deployments-from-bitbucket/), look config file and source code or simply ask me.

Project parameter for post deploy execution: `$PROJECTS['repo-name']['postHookCmd']` (see _config.sample.php_). For example, touch _index.wsgi_ for django configuration reloading: `... 'postHookCmd' => 'touch index.wsgi', ...`. Command running in project folder (specified by `deployPath` parameter.

## Cloning your repository

Now that SSH is configured you can clone your Bitbucket repository on your server. You might be tempted to clone the repository directly into the public website directory (for the sake of brevity we�ll call it www), but that approach comes with significant security risks. It requires that there is a .git folder inside of www, from which a malicious attacker could extract your entire website source code, possibly including database credentials and other sensitive information. Sure, you could use an .htaccess directive to hide the .git directory, but if that .htaccess file were accidentally deleted or edited you�d be left wide open.

A better approach is to store your git repository outside the public website directory, where it is hidden from prying eyes. If you create a bare repository then you can still checkout files to a detached working tree in www.

Before we begin, navigate to your repository on the Bitbucket website and copy its SSH URL. This will be in the format git@bitbucket.org:<username>/<repo-name>.git

Navigate the location on your server where you want to clone the repository. A good spot is probably one level above the www directory, which on my system is the website user�s home directory. At a shell prompt, clone your Bitbucket repository:

```
cd ~
git clone --mirror git@bitbucket.org:<username>/<repo-name>.git
```

Notice the --mirror flag? As its name implies this flag creates an exact mirror of the source repository, including mapping it�s remote branches. It implies --bare, which means that our repository will not have a working copy.

Your repository will be cloned into a directory called <repo-name>.git, in the user�s home directory. It doesn�t really matter what you name the directory, but suffixing the directory name with .git implies a bare repo, so it�s a useful naming convention to follow.

Now let�s do an initial checkout:

```
cd ~/<repo-name>.git
GIT_WORK_TREE=/home/<username>/www git checkout -f production
```

If this is first time you�ve communicated with bitbucket.org over SSH you may be prompted to accept Bitbucket�s server fingerprint, but if SSH is correctly configured you won�t be asked for your Bitbucket password or a key passphrase.

We have specified a GIT_WORK_TREE that corresponds to your public web directory, and checked out the production branch to that location. This step is important so that in future when our deployment script does a checkout we�re already on the correct branch.

Check that your initial checkout completed as expected, and that files from your production branch have been created in your public web directory. If everything worked as expected then you�re ready to set up automated deployments.

## Create a Bitbucket POST hook

## Deploying


